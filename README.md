# wdio-netent

Mocha step definitions written with WebdriverIO for test task using JS, WebdriverIO and Mocha+Chai.

# Framework installation

**Install node js**

[Node.js](http://nodejs.org/) version 8 or higher(latest stable is recommended):

- download here https://nodejs.org/en/download/

**Install Java 8**

- download here https://www.java.com/en/download/

**Install node-gyp if needed**

- download here https://github.org/nodejs/node-gyp

**Install xcode if needed**
To install xcode just run:

```
$ xcode-select --install
```

## Supported versions

[WebdriverIO](https://www.npmjs.com/package/webdriverio):

- 6.x

## Package installation

To install necessary packages just run:

```
$ npm install
```

## How to run application

To run application just run:

```
$ npm run start
```

## How to run the tests

To run tests just run:

```
$npm run test
```

# Configurations

To configure your tests, edit the `wdio.conf.js` file
