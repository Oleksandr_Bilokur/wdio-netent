import BasePage from './base.page.js';
import { expect } from 'chai';


class SlotPage extends BasePage {
    get btnSpin() { return $('//div[@id="start"]'); }
    get hdStatus() { return $('//h3[@id="status"]'); }
    get iconPulse() { return $$('//*[@class="pulse"]'); }
    get firstImage() { return $('//div[@id="result"]//div[1]'); }
    get divSymbols() { return $$('//div[@id="result"]//div'); }

    headerText() {
        return this.hdStatus.getText();
    }

    spinReel() {
        this.btnSpin.click();
    }

    checkStatusIsDispalyed() {
        return this.hdStatus.waitForDisplayed({ timeout: 3000 });
    }

    checkWin(status, pulseAmount, spins) {
        /* first parameter is status text 
        second parameter - amount of expected 'pulsed' symbols
        third parameter - amount of spins during which expected result should be reached */
        while (true && spins) {
            this.spinReel();
            if (this.checkStatusIsDispalyed() && this.headerText() === status) { // 3000ms as common wait, current spin timer is 1000ms, so if spin will be changed need to update or map with increment
                expect(this.iconPulse.length, `'${status}' should have ${pulseAmount} 'pulse' class(es)`).to.be.equal(pulseAmount);
                browser.pause(500); //pause just to visually show that it meets the expected result
                break;
            }
            spins--;
            expect(spins, `'${status}' status is not not reached`).to.be.greaterThan(0);
        }
    }

    checkImage(imagesName, spins) {
        /* first parameter is array with expected symbols image name
        second parameter - amount of spins during which expected result should be reached */
        const regexp = /[.\w]*png/g;
        let spinsCount = spins;
        let symbols = imagesName;
        while (symbols.length && spinsCount) {
            this.spinReel();
            if (this.checkStatusIsDispalyed()) {
                this.divSymbols.forEach(symbol => {
                    let index = symbols.indexOf(
                        symbol.getCSSProperty('background-image').value.
                            match(regexp).join()
                    );
                    let validSymbol = symbols.length && index + 1;
                    if (validSymbol) {
                        symbols.splice(index, 1);
                    }
                })
            }
            spinsCount--;
        }
        expect(symbols, `${symbols.join()} symbols is/are not present after ${spins} spins`).to.be.empty;
    }
}

export default new SlotPage();