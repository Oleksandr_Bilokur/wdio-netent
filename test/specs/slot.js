import slotPage from '../pageObjects/slot.page.js';
import { expect } from 'chai';


describe('[Desktop] E2E - Slot game page', () => {
    before('Open home page', () => {
        slotPage.open('/');
    });

    beforeEach('Refresh page to default state', () => {
        browser.refresh();
    });

    it('Check header text', () => {
        expect(slotPage.headerText(), 'Home page should have "Welcome" text in header').to.be.equal('Welcome');
    });

    it('Check case with "No Win, try again."', () => {
        slotPage.checkWin('No Win, try again.', 0, 20);
    });

    it('Check case with "Small win, try again to win more."', () => {
        slotPage.checkWin('Small win, try again to win more.', 2, 20);
    });

    it('Check case with "Big win, congratulations."', () => {
        slotPage.checkWin('Big win, congratulations.', 3, 20);
    });

    it('Check slot images', () => {
        slotPage.checkImage(['symbol_0.png', 'symbol_1.png', 'symbol_2.png', 'symbol_3.png', 'symbol_4.png', 'symbol_5.png'], 10);
    });
});